package com.example.myapplication4;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DonasiFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_donasi, container, false);

        RecyclerView rckebaikan = v.findViewById(R.id.recyclerview_kebaikan);
        RecyclerView rcpenggalangan = v.findViewById(R.id.recyclerview_penggalangan);
        RecyclerView rckategori = v.findViewById(R.id.recyclerview_kategori);

        LinearLayoutManager horizontalLayoutManager3
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rckategori.setLayoutManager(horizontalLayoutManager3);
        rckategori.setAdapter(new AdapterKategori(getActivity()));

        LinearLayoutManager horizontalLayoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rckebaikan.setLayoutManager(horizontalLayoutManager);
        rckebaikan.setAdapter(new AdapterKebaikan(getActivity()));

        LinearLayoutManager horizontalLayoutManager2
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rcpenggalangan.setLayoutManager(horizontalLayoutManager2);
        rcpenggalangan.setAdapter(new AdapterPenggalangan(getActivity()));

        return v;

    }
}