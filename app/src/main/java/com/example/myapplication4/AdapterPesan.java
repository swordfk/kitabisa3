package com.example.myapplication4;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterPesan extends RecyclerView.Adapter<AdapterPesan.viewHolder> {
    Context context;

    public AdapterPesan(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_pesan, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setGravity(Gravity.BOTTOM);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                dialog.setContentView(R.layout.dialog_pesan);

                TextView text1 = dialog.findViewById(R.id.text1);
                TextView text2 = dialog.findViewById(R.id.text2);
                LinearLayout layout1 = dialog.findViewById(R.id.layout1);
                LinearLayout layout2 = dialog.findViewById(R.id.layout2);

                text1.setTextColor(Color.parseColor("#000000"));
                text2.setTextColor(Color.parseColor("#989898"));
                layout1.setVisibility(View.VISIBLE);
                layout2.setVisibility(View.GONE);

                text1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        text1.setTextColor(Color.parseColor("#000000"));
                        text2.setTextColor(Color.parseColor("#989898"));
                        layout1.setVisibility(View.VISIBLE);
                        layout2.setVisibility(View.GONE);
                    }
                });
                text2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        text2.setTextColor(Color.parseColor("#000000"));
                        text1.setTextColor(Color.parseColor("#989898"));
                        layout2.setVisibility(View.VISIBLE);
                        layout1.setVisibility(View.GONE);
                    }
                });

                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        public viewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
