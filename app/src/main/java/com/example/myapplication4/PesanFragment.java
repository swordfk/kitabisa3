package com.example.myapplication4;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PesanFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_pesan, container, false);
        RecyclerView rcRekomen = v.findViewById(R.id.recyclerview_rekomen);
        rcRekomen.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcRekomen.setAdapter(new AdapterPesan(getActivity()));

        return  v;
    }
}