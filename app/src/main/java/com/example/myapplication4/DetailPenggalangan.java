package com.example.myapplication4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DetailPenggalangan extends AppCompatActivity {
    boolean storie = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_penggalangan);

        RecyclerView rcStories = findViewById(R.id.rc_stories);
        CardView dana = findViewById(R.id.cd_dana);
        LinearLayout stories = findViewById(R.id.txtStorie);
        LinearLayout danas = findViewById(R.id.txtDana);
        LinearLayout faq = findViewById(R.id.lay_faq);
        CardView bulets = findViewById(R.id.bulets);
        CardView buletd = findViewById(R.id.buletd);
        TextView tvstorie = findViewById(R.id.tvStorie);
        TextView tvdana = findViewById(R.id.tvDana);
        TextView lengkap = findViewById(R.id.delengkapnya);

        lengkap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), DetailStory.class));
            }
        });

        faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), TentangAct.class));
            }
        });

        stories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rcStories.setVisibility(View.VISIBLE);
                dana.setVisibility(View.GONE);
                bulets.setVisibility(View.VISIBLE);
                buletd.setVisibility(View.INVISIBLE);
                tvstorie.setTextColor(Color.parseColor("#000000"));
                tvdana.setTextColor(Color.parseColor("#989898"));
            }
        });

        danas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rcStories.setVisibility(View.GONE);
                dana.setVisibility(View.VISIBLE);
                bulets.setVisibility(View.INVISIBLE);
                buletd.setVisibility(View.VISIBLE);
                tvdana.setTextColor(Color.parseColor("#000000"));
                tvstorie.setTextColor(Color.parseColor("#989898"));
            }
        });

        rcStories.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        rcStories.setAdapter(new AdapterStories(this));

        if (storie){
            rcStories.setVisibility(View.VISIBLE);
            dana.setVisibility(View.GONE);
            bulets.setVisibility(View.VISIBLE);
            buletd.setVisibility(View.INVISIBLE);
            tvstorie.setTextColor(Color.parseColor("#000000"));
            tvdana.setTextColor(Color.parseColor("#989898"));
        }



    }
}